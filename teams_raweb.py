# Get all team in raweb and check if already exist
from elasticsearch import Elasticsearch
import logging, configparser
from pymongo import MongoClient
import configparser
# Config properties
config = configparser.RawConfigParser()
config.read("ConfigFile.properties")

# Logger configuration
logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__file__.split('/')[-1])

mongodb_ip = config.get("mongodb", "ip")
mongodb_port = int(config.get("mongodb", "port"))
mongodb_db = config.get("mongodb", "database")
es = Elasticsearch(config.get("elasticsearch", "ip"))
index_team = config.get("elasticsearch", "index_team")
index_pub = config.get("elasticsearch", "index_pub")

# MongoDB client
client = MongoClient(mongodb_ip, mongodb_port)

def get_pub_of_team(acronym) :
    """
        Get publications for team acronym
    """
    get_pubs = {
    "size" : 10000,
                "query" : {
                    "bool": {
                        "must": [
                           {"nested": {
                           "path": "affiliations",
                           "query": { "match": {
                            "affiliations.acronym" : acronym
                        }}}}
                ]
            }
        }
    }
    return es.search(index=index_pub, body=get_pubs)

def get_start_stop_resp(acronym) :
    teams_acronym = db.teams.find({'acronym' : acronym})
    date_start = 3000
    date_end = 0
    responsable = ""
    for year in teams_acronym :
        if year["year"] < date_start :
            date_start = year["year"]
        if year["year"] > date_end :
            date_end = year["year"]
            for member in year["members"] :
                if member["category"] == "responsable scientifique" or member["category"] == "head of project team" :
                    responsable = member["firstname"] + " " + member["lastname"]
    return [date_start, date_end, responsable]

if __name__ == '__main__':
    clean_team_list = {"magrite", "miro", "métalau", "virtual_plants", "virtualplants", "eptar", "congé", "euréca", "opéra", "langue_et_dialogue",
                        "psychoergo", "pop_art", "opéra/rhône-alpes", "popart", "sémagramme", "résédas", "méta2", "méval", "sigma2", "sigma 2", "méta 2",
                        "ep-atr", "epatr", "ep ­atr" }
    db = client.get_database(mongodb_db)
    db.teams_chronogram.drop()
    teams = db.teams.distinct("acronym")
    teams.sort()
    k = 0
    for team in teams:
        if team in clean_team_list :
            if "é" in team :
                team = team.replace("é", "e")
            if "_" in team :
                team = team.replace("_", " ")
            if "/" in team :
                team = team.split("/")[0]
            if team == "magrite" :
                team = "magrit"
            if team == "miro" :
                team = "mirho"
            if team == "virtualplants" :
                team = "virtual plants"
            if team == "popart" :
                team = "pop art"
            if team == "meta 2" :
                team = "meta2"
            if team == "sigma 2" :
                team = "sigma2"
        if team == "psycho­ergo" or team == "psycho-ergo" :
            team = "psycho­ ergo"
        if team == "vasy-ra" :
            team = "vasy"
        if team == "movi-grenoble" :
            team = "movi"
        if team == "ep-atr" or team == "epatr" or team == "eptar" or team == "ep ­atr" or team == "ep­atr":
            team = "epatr"
        if team == "langue et\n      dialogue" :
            team == "langue et dialogue"
        query = { "query" : { "match" : {"acronym" : team.upper()}}}
        result = es.search(index = index_team, body=query)
        new_info = {}
        if result["hits"]["total"]["value"] == 0 :
            pubs = get_pub_of_team(team.upper())
            if pubs["hits"]["total"]["value"] != 0 :
                i = 0
                all_pubs = []
                for pub in pubs["hits"]["hits"] :
                    if i == 0 :
                        for aff in pub["_source"]["affiliations"]:
                            if aff["acronym"] == team.upper() :
                                new_info["id"] = aff["id"]
                                new_info["type"] = aff["type"]
                                new_info["address"] = aff["address"]
                                new_info["status"] = aff["status"]
                                new_info["country_key"] = aff["country_key"]
                                new_info["name"] = aff["name"]
                                new_info["url"] = aff["url"]
                                new_info["acronym"] = aff["acronym"]
                        i+=1
                    for aff in pub["_source"]["affiliations"] :
                        if aff["acronym"] == new_info["acronym"] :
                            modif = False
                            for rel in aff["relations"]["indirect"] :
                                if rel["acronym"] == "INRIA" :
                                    del pub["_source"]["affiliations"]
                                    del pub["_source"]["projects"]
                                    all_pubs.append(pub["_source"])
                                    modif = True
                                    break
                            if modif :
                                break
                new_info["pubs"] = all_pubs
                [new_info["date_start"],new_info["date_end"],new_info["responsable"]] = get_start_stop_resp(team)
                new_info["date_start"] = str(new_info["date_start"])
                new_info["date_end"] = str(new_info["date_end"])
                es.index(index=index_team, id=new_info["id"], body=new_info)
            else :
                k = k + 1
                new_info["status"] = "WAIT"
                new_info["acronym"] = team.upper()
                [new_info["date_start"],new_info["date_end"],new_info["responsable"]] = get_start_stop_resp(team)
                new_info["date_start"] = str(new_info["date_start"])
                new_info["date_end"] = str(new_info["date_end"])
                es.index(index=index_team, id="wait"+str(k), body=new_info)
