import requests
import json
import csv
import re
import os
from html.parser import HTMLParser
from elasticsearch import Elasticsearch
from bs4 import BeautifulSoup
import configparser
import datetime

def get_csv_from_bastri() :
    """
        Get csv file from bastri, Create or Update team
    """
    cwd = os.getcwd()
    array_all_bastri = []
    with open(cwd + '/test.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
        dico = {}
        i = 0
        reopen = 2
        for row in spamreader:
            i = i +1
            if i > 1 :
                array_all_bastri.append(row)
    return array_all_bastri

def manage_new_team(csv_row) :
    i = 0
    all_team = []
    for info_team in csv_row :
        acronym = info_team[1]
        team = get_team(acronym)
        if team["hits"]["total"]["value"] == 0 :
            # CREATE
            create_team(info_team)
            all_team.append(info_team[0])
        else :
            # UPDATE
            update_team(team["hits"]["hits"][0], info_team)
        i+=1
    return all_team

def get_team(name) :
    """
        Get team in elasticsearch
    """
    query = {
    "query" : {"match" : {
        "acronym.keyword" : name
    }}
    }
    return es.search(index = index_team, body=query)

def update_team(team_source, row) :
    """
        Update info in team if info change in bastri
    """
    update_info = {}
    modif = False
    if row[1] != team_source["_source"]["acronym"] :
        update_info["acronym"] = row[1]
        modif = True
    if row[21].split("/")[2] < team_source["_source"]["date_start"].split("/")[2] :
        update_info["date_start"] = row[21]
        modif = True
    if row[22].split("/")[2] > team_source["_source"]["date_end"].split("/")[2] :
        update_info["date_end"] = row[22]
        modif = True
    if row[6] != team_source["_source"]["cri_bastri"] :
        update_info["cri_bastri"] = row[6]
        modif = True
    if row[7] != team_source["_source"]["site"] :
        update_info["site"] = row[7]
        modif = True
    if row[4] + " " + row[5] != team_source["_source"]["responsable"] :
        update_info["responsable"] = row[4] + " " + row[5]
        modif = True
    if modif :
        es.update(index=index_team, id=team_source["_id"], body={"doc" : update_info})

def create_team(info_team) :
    """
        Create new team for Elasticsearch (New team in bastri)
    """
    new_info = {}
    new_info["acronym"] = info_team[1]
    new_info["date_start"] = info_team[21]
    new_info["date_end"] = info_team[22]
    new_info["cri_bastri"] = info_team[6]
    new_info["site"] = info_team[7]
    new_info["responsable"] = info_team[4] + " " + info_team[5]
    team_pubs = get_pub_of_team(new_info["acronym"])
    i = 0
    all_pubs = []
    now = datetime.datetime.now()
    if team_pubs["hits"]["total"]["value"] != 0 :
        for pub in team_pubs["hits"]["hits"] :
            if i == 0 :
                for aff in pub["_source"]["affiliations"]:
                    if aff["acronym"] == new_info["acronym"] :
                        new_info["id"] = aff["id"]
                        new_info["type"] = aff["type"]
                        new_info["address"] = aff["address"]
                        if not info_team[22] :
                            new_info["status"] = "OLD"
                        elif int(info_team[22].split("/")[2]) < now.year :
                            new_info["status"] = "OLD"
                        else :
                            new_info["status"] = "VALID"
                        new_info["country_key"] = aff["country_key"]
                        new_info["name"] = aff["name"]
                        new_info["url"] = aff["url"]
                i+=1
            for aff in pub["_source"]["affiliations"] :
                if aff["acronym"] == new_info["acronym"] :
                    modif = False
                    for rel in aff["relations"]["indirect"] :
                        if rel["acronym"] == "INRIA" :
                            del pub["_source"]["affiliations"]
                            del pub["_source"]["projects"]
                            all_pubs.append(pub["_source"])
                            modif = True
                            break
                    if modif :
                        break
        new_info["pubs"] = all_pubs
        print(len(all_pubs))
        print(new_info["acronym"])
        try :
            es.create(index=index_team, id=new_info["id"], body=new_info, refresh='wait_for')
        except Exception as e :
            print(e)
            print(new_info["acronym"])
    else :
        if int(info_team[22].split("/")[2]) < now.year :
            new_info["status"] = "OLD"
        else :
            new_info["status"] = "WAIT"
        es.index(index=index_team, id=info_team[0], body=new_info)

def create_history_bastri(siid) :
    """
        Create history of the team from bastri in a dictionary
    """
    r = requests.get('https://bastri.inria.fr/FichesEquipes/structurerecherche/show/'+siid)
    parsed_html = BeautifulSoup(r.text, 'lxml')
    text = parsed_html.find("p", {"class" : "genealogie"}).text
    hist = str(re.findall("[^\n\t' '()\xa0→]\w*", text))
    text = hist.replace('[', '')
    text = text.replace(']', '')
    text = text.replace("'", '')
    text = text.replace("'", '')
    all_team = text.split(',')
    allteam_strip = []
    if len(all_team) > 1 :
        i = 0
        j = 1
        for val in all_team :
            if i == 0 :
                name = val.strip()
            elif name == val.strip() :
                j = j + 1
            i = i +1
        if j != len(all_team) :
            allteam_strip = []
            i = 0
            for valnotstrip in all_team :
                containvalue = valnotstrip.strip()
                if "-" in containvalue :
                    containvalue = all_team[i - 1].strip() + containvalue
                    del allteam_strip[len(allteam_strip)- 1]
                elif ".R2" in containvalue :
                     containvalue = "PI.R2"
                elif ".H" in containvalue :
                    containvalue = "S.H.A.M.A.N"
                    del allteam_strip[len(allteam_strip)- 1]
                elif containvalue == "VIRTUAL" or containvalue == "POP":
                    containvalue + all_team[i + 1].strip()
                    del all_team[i + 1]
                if containvalue not in allteam_strip or containvalue == '' :
                    if containvalue != ".A" and containvalue != ".M" and containvalue != ".N" :
                        allteam_strip.append(containvalue)
                i = i + 1
            allteam_strip
    else :
        allteam_strip = []
    for a in allteam_strip :
        if 'SR0' in a :
            allteam_strip.remove(a)
    return allteam_strip

def create_team_history_for_elastic(key, value) :
    """
        Create team history to index in Elasticsearch
    """
    r = requests.get('https://bastri.inria.fr/FichesEquipes/structurerecherche/show/'+key)
    parsed_html = BeautifulSoup(r.text, 'lxml')
    t = parsed_html.findAll("h1")[1].text
    first_acro = re.findall('\w*', t)[0]
    text = parsed_html.findAll("p", {"class": "genealogie"})[0].text
    newt = re.sub('[^\w\(\)\→\,\.]+', ' ',text)
    text = re.findall("[\w\(\)\→\,\.]+", newt)
    team = get_team(first_acro)
    if len(team["hits"]["hits"]) != 0 :
        team_to_update = team["hits"]["hits"][0]
        team = team["hits"]["hits"][0]["_source"]
        old_acronym = []
        history_bastri = []
        for acronym in text :
            if acronym not in old_acronym :
                team_2 = get_team(acronym)
                if len(team_2["hits"]["hits"]) != 0 :
                    history_bastri.append({"name" : team_2["hits"]["hits"][0]["_source"]["acronym"], "id_struct" : team_2["hits"]["hits"][0]["_id"], "status" : team_2["hits"]["hits"][0]["_source"]["status"]})
                elif "(SR" not in acronym :
                    history_bastri.append({"name" : acronym, "id_struct" : "", "status" : "OLD"})
                if acronym != '→' :
                    old_acronym.append(acronym)
        past_for = False
        id_to_del = []
        for i in range(0, len(history_bastri)) :
            if history_bastri[i]["name"] == '(' and history_bastri[i+1]["name"] == ',' :
                id_to_del.append(i)
                id_to_del.append(i+1)
                past_for = True
            if i < len(history_bastri)-1 :
                if history_bastri[i]["name"] == '→' and history_bastri[i+1]["name"] == '→' :
                    id_to_del.append(i)
            if past_for and history_bastri[i]["name"] == ')':
                id_to_del.append(i)
        id_to_del.reverse()
        for id_del in id_to_del :
            del history_bastri[id_del]
        if len(history_bastri) == 2 :
            team["history_bastri"] = history_bastri[0]
        else :
            team["history_bastri"] = history_bastri
        es.index(index=index_team, id=team_to_update["_id"], body=team)
        # if '' in value :
        #     if text == team["acronym"] :
        #         history_bastri = []
        #         i = len(value) - 1
        #         for val in reversed(value) :
        #                 if val != '' :
        #                     team_2 = get_team(val)
        #                     if len(team_2["hits"]["hits"]) != 0 :
        #                         team_2 = team_2["hits"]["hits"][0]["_source"]
        #                         if val == team_2["acronym"] :
        #                             if i == len(value) - 1 :
        #                                 if "id" in team_2 :
        #                                     history_bastri.append({"name" : team_2["acronym"], "id_struct" : team_2["id"], "split" : "Y", "status" : team_2["status"]})
        #                                 else :
        #                                     history_bastri.append({"name" : team_2["acronym"], "id_struct" : "", "split" : "Y", "status" : team_2["status"]})
        #                             elif value[i + 1] == '' :
        #                                 if "id" in team_2 :
        #                                     history_bastri.append({"name" : team_2["acronym"], "id_struct" : team_2["id"], "split" : "Y", "status" : team_2["status"]})
        #                                 else :
        #                                     history_bastri.append({"name" : team_2["acronym"], "id_struct" : "", "split" : "Y", "status" : team_2["status"]})
        #                             else :
        #                                 if "id" in team_2 :
        #                                     history_bastri.append({"name" : team_2["acronym"], "id_struct" : team_2["id"], "split" : "N", "status" : team_2["status"]})
        #                                 else :
        #                                     history_bastri.append({"name" : team_2["acronym"], "id_struct" : "", "split" : "N", "status" : team_2["status"]})
        #                     else :
        #                         if i == len(value) - 1 :
        #                             history_bastri.append({"name" : val, "id_struct" : "", "split" : "Y", "status" : "WAIT"})
        #                         elif value[i + 1] == '' :
        #                             history_bastri.append({"name" : val, "id_struct" : "", "split" : "Y", "status" : "WAIT"})
        #                         else :
        #                             history_bastri.append({"name" : val, "id_struct" : "", "split" : "N", "status" : "WAIT"})
        #                 i = i - 1
        #         history_bastri.reverse()
        #         team["history_bastri"] = history_bastri
        #         es.index(index=index_team, id=team_to_update["_id"], body=team)
        #     else :
        #         print("Not in found " + value[0])
        # else :
        #     if text == team["acronym"] :
        #         history_bastri = []
        #         i = len(value) - 1
        #         for val in value :
        #                 team_2 = get_team(val)
        #                 if len(team_2["hits"]["hits"]) != 0 :
        #                     team_2 = team_2["hits"]["hits"][0]["_source"]
        #                     if val == team_2["acronym"] :
        #                         if "id" in team_2 :
        #                             history_bastri.append({"name" : team_2["acronym"], "id_struct" : team_2["id"], "split" : "N", "status" : team_2["status"]})
        #                         else :
        #                             history_bastri.append({"name" : team_2["acronym"], "id_struct" : "", "split" : "N", "status" : team_2["status"]})
        #                 else :
        #                     if i == len(value) - 1 :
        #                         history_bastri.append({"name" : val, "id_struct" : "", "split" : "Y", "status" : "WAIT"})
        #                     elif value[i + 1] == '' :
        #                         history_bastri.append({"name" : val, "id_struct" : "", "split" : "Y", "status" : "WAIT"})
        #                     else :
        #                         history_bastri.append({"name" : val, "id_struct" : "", "split" : "N", "status" : "WAIT"})
        #         team["history_bastri"] = history_bastri
        #         es.index(index=index_team, id=team_to_update["_id"], body=team)
    else :
        print("Not in Elastic " + first_acro)

def get_pub_of_team(acronym) :
    """
        Get publications for team acronym
    """
    get_pubs = {
    "size" : 10000,
                "query" : {
                    "bool": {
                        "must": [
                           {"nested": {
                           "path": "affiliations",
                           "query": { "match": {
                            "affiliations.acronym" : acronym
                        }}}}
                ]
            }
        }
    }
    return es.search(index=index_pub, body=get_pubs)


def update_wait_team() :
    """
        Update Team where no publications was found in last update
    """
    query_wait = {"query" : {"match" : {"status" : "WAIT"}} }
    wait_team = es.search(index=index_team, body=query_wait)
    for team in wait_team["hits"]["hits"] :
        team_pubs = get_pub_of_team(team["_source"]["acronym"])
        i = 0
        all_pubs = []
        if team_pubs["hits"]["total"]["value"] != 0 :
            for pub in team_pubs["hits"]["hits"] :
                all_pubs.append(pub["_source"])
                if i == 0 :
                    for aff in pub["_source"]["affiliations"]:
                        if aff["acronym"] == team["_source"]["acronym"] :
                            team["_source"]["id"] = aff["id"]
                            team["_source"]["type"] = aff["type"]
                            team["_source"]["adress"] = aff["adress"]
                            team["_source"]["status"] = aff["status"]
                            team["_source"]["country_key"] = aff["country_key"]
                            team["_source"]["name"] = aff["name"]
                            team["_source"]["url"] = aff["url"]

                    i+=1
            team["_source"]["pubs"] = all_pubs
            es.index(index=index_team, id=team["_source"]["id"], body=team["_source"])
            es.delete(index=index_team,  id=team["_id"])

if __name__ == '__main__':
    config = configparser.RawConfigParser()
    config.read("ConfigFile.properties")
    es = Elasticsearch(config.get("elasticsearch", "ip"))
    index_team = config.get("elasticsearch", "index_team")
    index_pub = config.get("elasticsearch", "index_pub")
    csv_file = get_csv_from_bastri()
    team_history = manage_new_team(csv_file)
    for team in team_history :
        # bastri_history = create_history_bastri(team)
        create_team_history_for_elastic(team, [])
    # update_wait_team()
